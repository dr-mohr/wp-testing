<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <?php
    get_template_part( 'views/header/head' );
    wp_head();
    ?>
</head>

<body class="pt-20 antialiased font-sans leading-normal text-gray-900">
    <div id="app">
        <?php get_template_part( 'views/header/header' ); ?>