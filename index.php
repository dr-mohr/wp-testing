<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<main class="pb-24">

    <section class="mt-16">
        <div class="container mx-auto px-4">

            <div class="-mx-4">
                <div class="px-4 w-5/12">
                    <article data-id="<?php the_ID(); ?>">
                        <h1 class="page-title text-4xl font-bold leading-tight">
                            <?php the_title(); ?>
                        </h1>
                        <div class="content">
                            <?php the_content(); ?>
                        </div>
                    </article>
                </div>
            </div>

        </div>
    </section>

</main>

<?php endwhile; else : ?>
<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php get_footer(); ?>