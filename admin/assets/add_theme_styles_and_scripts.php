<?php

function add_theme_styles_and_scripts() {
    wp_enqueue_style( 'app', get_template_directory_uri() . '/css/app.css' );
    wp_enqueue_script( 'app', get_template_directory_uri() . '/js/app.js', [], '1.0.0', true );
}