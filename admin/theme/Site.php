<?php

namespace Admin\Theme;

class Site {

    public function add_theme_support(array $items): void
    {
        foreach($items as $feature) {
            add_theme_support($feature);
        }
    }

    public function remove_action(array $items): void
    {
        foreach($items as $target => $feature) {
            remove_action($target, $feature);
        }
    }

}