<?php

class Leistung
{
    public static function init()
    {
        function create_post_type()
        {
            register_post_type(
      'leistungen',
    [
      'labels' => [
        'name' => __('Leistungen'),
        'singular_name' => __('Leistung'),
        'menu_name'          => __('Leistungen'),
        'name_admin_bar'     => __('Leistung', 'add new on admin bar'),
        'add_new'            => __('Weitere hinzufügen', 'leistung'),
        'add_new_item'       => __('Neue Leistung hinzufügen'),
        'new_item'           => __('Neue Leistung'),
        'edit_item'          => __('Leistung bearbeiten'),
        'view_item'          => __('Leistung ansehen'),
        'all_items'          => __('Alle Leistungen'),
        'search_items'       => __('Leistungen suchen'),
        'parent_item_colon'  => __('Eltern-Leistungen:'),
        'not_found'          => __('Keine Leistungen gefunden.'),
        'not_found_in_trash' => __('Keine Leistungen im Mülleimer.'),
      ],
      'public' => true,
      'has_archive' => true,
      'rewrite' => ['slug' => 'leistungen'],
    ]
  );
        }
        add_action('init', 'create_post_type');
    }
}
