import ExampleComponent from './components/ExampleComponent.vue';

window.Vue = require('vue');

Vue.component('example-component', ExampleComponent);

new Vue({
    el: '#app'
});