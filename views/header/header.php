<header class="fixed top-0 w-full bg-white z-30 header shadow">
    <div class="container mx-auto px-4">
        <div class="h-20 flex justify-between items-center">
            <div class="header__logo">
                <?php if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) : ?>
                    <?php the_custom_logo(); ?>
                <?php else : ?>
                <a class="hover:underline text-2xl font-bold" href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a>
            </div>
            <?php endif; ?>
            <nav class="nav">
            <?php wp_nav_menu([ 'theme_location' => 'primary_menu' ]); ?>
            </nav>
        </div>
    </div>
</header>