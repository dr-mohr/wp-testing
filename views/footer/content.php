<footer class="border-t-2 pt-12 footer">

    <div class="container mx-auto px-4">
        <div class="-mx-4 lg:flex lg:flex-wrap">
            <div class="px-4 lg:w-1/3">Widget Area 1</div>
            <div class="px-4 lg:w-1/3">Widget Area 2</div>
            <div class="px-4 lg:w-1/3">Widget Area 3</div>
        </div>
    </div>

</footer>