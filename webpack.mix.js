const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

const page = {
    localUrl: 'http://wordpress.test/',
    watchables: [
        './css/*.css',
        './js/*.js',
        './**/*.php'
    ]
};

mix.browserSync({
    proxy: page.localUrl,
    files: page.watchables
});

mix.setPublicPath('./');

mix.js('resources/js/app.js', './js')
    .sass('resources/sass/app.scss', './css').options({
        processCssUrls: false,
        postCss: [tailwindcss('./tailwind.config.js')],
    });